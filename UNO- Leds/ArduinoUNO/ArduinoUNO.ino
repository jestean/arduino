int a = 1;
void setup(){
  do{
  pinMode(a,OUTPUT);
  a++;
  }while(a < 10);
}

void loop(){
  a = 1;
  do{
    digitalWrite(a,HIGH);
    delay(500);
    digitalWrite(a,LOW);
    a++;
  } while(a < 9);
  
  for(a;a > 0; a--){
    digitalWrite(a,HIGH);
    delay(1000);
    digitalWrite(a,LOW);
  }
}
